# eBook Repository - Front-end

University project mainly focused on successful implementation of Elastic Search with web support.
Elastic search was the main topic due to its recent popularity as a fast searching option used in enterprise systems.

## Basic functionallity

Predefined user for testing and general organization is user: admin, password: admin with ADMIN rights.
Only admins and managers can register other users to the system.

Managers and ranks above him can manage other users, their subscriptions as well as post new eBooks or delete existing ones.

Once the user is being added to the system, they can have specific book category subscription added for them,
if nothing is specified, the user will be registered for all books.
Logged in users can browse through all eBooks, but can only download the ones they are subscribed for.

## Server side

This project represents only front-end side of the overall project, server side can be found here:
https://gitlab.com/aleksandarGreat/eBookRepository

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README]
(https://github.com/angular/angular-cli/blob/master/README.md).