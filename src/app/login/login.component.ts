import {Component, OnInit} from '@angular/core';
import {RestService} from '../rest.service';
import {LoginDTO} from '../dto/login.model';
import {Router} from '@angular/router';
import {DataService} from '../data.service';
import {User} from '../model/user.model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  username: string;
  password: string;
  error = false;

  constructor(private restService: RestService, private router: Router, private dataService: DataService) {
  }

  ngOnInit() {
  }

  login() {
    this.restService.login(new LoginDTO(this.username, this.password)).subscribe(
      res => {
        console.log(res.lastName);
        if (this.router.navigate([''])) {
          if (res.category != null) {
            this.dataService.user = new User(res.id, res.firstName, res.lastName, res.username, res.type, res.category.name);
          } else {
            this.dataService.user = new User(res.id, res.firstName, res.lastName, res.username, res.type, null);
          }
          this.dataService.loggedIn = true;
        }
      },
      err => {
        this.error = true;
        this.dataService.loggedIn = false;
      }
    );
  }

}
