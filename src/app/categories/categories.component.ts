import {Component, OnInit} from '@angular/core';
import {RestService} from '../rest.service';
import {CategoryDTO} from '../dto/categoryDTO.model';
import {CreateCategoryDTO} from '../dto/createCategoryDTO.model';
import {UpdateCategoryDTO} from '../dto/updateCategoryDTO.model';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss']
})
export class CategoriesComponent implements OnInit {

  categories: Array<CategoryDTO> = [];

  addCategory = false;
  updateCategory = false;

  name: string;

  id: number;
  updateName: string;

  constructor(private restService: RestService) {
  }

  ngOnInit() {
    this.getAllCategories();
  }

  private getAllCategories() {
    this.restService.getAllCategories().subscribe(
      res => {
        this.categories = res;
      },
      err => {
        console.log(err);
      }
    );
  }

  delete(id: number) {
    this.restService.deleteCategory(id).subscribe(
      res => {
        console.log(res);
        this.getAllCategories();
      },
      err => {
        console.log(err);
      }
    );
  }

  cancel() {
    this.addCategory = false;
    this.updateCategory = false;
  }

  add() {
    this.addCategory = true;
  }

  createCategory() {
    const createCategoryDTO = new CreateCategoryDTO(this.name);
    this.restService.createCategory(createCategoryDTO).subscribe(
      res => {
        console.log(res);
        this.getAllCategories();
        this.addCategory = false;
      },
      err => {
        console.log(err);
      }
    );
  }

  update(categoryDTO: CategoryDTO) {
    this.updateCategory = true;
    this.id = categoryDTO.id;
    this.updateName = categoryDTO.name;
  }

  changeCategory() {
    const updateCategoryDTO = new UpdateCategoryDTO(this.id, this.updateName);
    this.restService.updateCategory(updateCategoryDTO).subscribe(
      res => {
        console.log(res);
        this.getAllCategories();
        this.updateCategory = false;
      },
      err => {
        console.log(err);
      }
    );
  }
}
