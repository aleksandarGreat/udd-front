import {Component, OnInit} from '@angular/core';
import {RestService} from '../rest.service';
import {BookDTO} from '../dto/book.model';
import {DataService} from '../data.service';
import {User} from '../model/user.model';
import {saveAs} from 'file-saver';
import {CategoryDTO} from '../dto/categoryDTO.model';
import {UpdateBookDTO} from '../dto/updateBookDTO.model';
import {QueryDTO} from '../dto/queryDTO.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  books: Array<BookDTO> = [];
  categories: Array<CategoryDTO> = [];
  error = false;
  user: User;

  queryDTO = new QueryDTO();

  addBook = false;
  bookUpdate = false;

  categoryId: number;
  languageId: number;
  file: File;
  fileLabel = 'Choose file';

  updateBookDTO = new UpdateBookDTO();

  private static wait(ms: number) {
    const start = new Date().getTime();
    let end = start;
    while (end < start + ms) {
      end = new Date().getTime();
    }
  }

  constructor(private restService: RestService, public dataService: DataService) {
  }

  ngOnInit() {
    this.user = this.dataService.user;
    this.loadBooks();
    this.loadCategories();
  }

  loadBooks() {
    this.restService.getAllBooks().subscribe(
      res => {
        this.error = false;
        this.books = res;
      },
      err => {
        console.log(err);
        this.error = true;
        HomeComponent.wait(7000);
        this.loadBooks();
      }
    );
  }

  loadCategories() {
    this.restService.getAllCategories().subscribe(
      res => {
        this.categories = res;
      },
      err => {
        console.log(err);
      }
    );
  }

  search() {
    this.restService.searchBooks(this.queryDTO).subscribe(
      res => {
        if (this.queryDTO.content === null || this.queryDTO.content === undefined) {
          this.books = res;
        } else {
          const contentBooks: Array<BookDTO> = [];
          res.map(book => {
            const fragments = new RegExp('fragments', 'g');
            const openEm = new RegExp('<em>', 'g');
            const closeEm = new RegExp('</em>', 'g');
            if (book.content != null) {
              book.content = book.content.replace('[content],', '');
              book.content = book.content.replace(fragments, '');
              book.content = book.content.replace('[[', '');
              book.content = book.content.replace(']]', '');
              book.content = book.content.replace(openEm, '');
              book.content = book.content.replace(closeEm, '');
            }
            contentBooks.push(book);
          });
          this.books = contentBooks;
        }
      },
      err => {
        console.log(err);
      }
    );
  }

  cancel() {
    this.addBook = false;
    this.bookUpdate = false;
  }

  add() {
    this.addBook = true;
  }

  edit(book: BookDTO) {
    this.bookUpdate = true;
    this.updateBookDTO.id = book.id;
    this.updateBookDTO.title = book.title;
    this.updateBookDTO.author = book.author;
    this.updateBookDTO.keywords = book.keywords;
    this.updateBookDTO.publicationYear = book.publicationYear;
    this.updateBookDTO.mime = book.mime;
    this.updateBookDTO.categoryId = book.category.id;
    this.updateBookDTO.languageId = book.language.id;
  }

  setFiles(event) {
    this.fileLabel = 'Choose file';
    const files = event.srcElement.files;
    if (!files) {
      return;
    }
    this.file = files[0];
    this.fileLabel = files[0].name;
  }

  uploadBook() {
    const formData: FormData = new FormData();
    formData.append('file', this.file, this.file.name);
    formData.append('categoryId', this.categoryId.toString());
    formData.append('languageId', this.languageId.toString());
    console.log(formData);

    this.restService.uploadBook(formData).subscribe(
      res => {
        console.log(res);
        this.bookUpdate = true;
        this.addBook = false;
        this.updateBookDTO.id = res.id;
        this.updateBookDTO.title = res.title;
        this.updateBookDTO.author = res.author;
        this.updateBookDTO.keywords = res.keywords;
        this.updateBookDTO.publicationYear = res.publicationYear;
        this.updateBookDTO.mime = res.mime;
        this.updateBookDTO.categoryId = res.categoryId;
        this.updateBookDTO.languageId = res.languageId;
      },
      err => {
        console.log(err);
      }
    );
  }

  updateBook() {
    console.log(this.updateBookDTO);
    this.restService.updateBook(this.updateBookDTO).subscribe(
      res => {
        console.log(res);
        this.bookUpdate = false;
        this.loadBooks();
      },
      err => {
        console.log(err);
      }
    );
  }

  download(id: number) {
    this.restService.downloadBook(id).subscribe(
      res => {
        let filename = res.headers.get('Content-Disposition').split('filename=')[1];
        if (filename === 'null') {
          filename = 'book';
        }
        saveAs(new Blob([res.body], {type: 'application/pdf'}), filename);
      },
      err => {
        console.log(err);
      }
    );
  }

  delete(id: number) {
    this.restService.deleteBook(id).subscribe(
      res => {
        console.log(res);
        this.loadBooks();
      },
      err => {
        console.log(err);
      }
    );
  }
}
