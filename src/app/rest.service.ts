import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {LoginDTO} from './dto/login.model';
import {UserDTO} from './dto/useDTO.model';
import {BookDTO} from './dto/book.model';
import {UpdatePasswordDTO} from './dto/updatePasswordDTO.model';
import {UpdateUserDTO} from './dto/updateUserDTO.model';
import {CategoryDTO} from './dto/categoryDTO.model';
import {CreateUserDTO} from './dto/createUserDTO.model';
import {CreateCategoryDTO} from './dto/createCategoryDTO.model';
import {UpdateCategoryDTO} from './dto/updateCategoryDTO.model';
import {UpdateBookDTO} from './dto/updateBookDTO.model';
import {UploadBookDTO} from './dto/uploadBookDTO.model';
import {QueryDTO} from './dto/queryDTO.model';
import {ChangeUserDTO} from './dto/changeUserDTO.model';

@Injectable()
export class RestService {

  private url = 'http://localhost:8082/api';

  constructor(private httpClient: HttpClient) {

  }

  login(loginDto: LoginDTO) {
    return this.httpClient.post<UserDTO>(this.url + '/login', loginDto);
  }

  getAllBooks() {
    return this.httpClient.get<BookDTO[]>(this.url + '/books/all');
  }

  searchBooks(queryDTO: QueryDTO) {
    return this.httpClient.put<BookDTO[]>(this.url + '/queries/query', queryDTO);
  }

  uploadBook(formData: FormData) {
    return this.httpClient.post<UploadBookDTO>(this.url + '/books/upload', formData);
  }

  updateBook(updateBookDTO: UpdateBookDTO) {
    return this.httpClient.put(this.url + '/books/update', updateBookDTO);
  }

  downloadBook(id: number) {
    return this.httpClient.get(this.url + '/books/download/' + id, {responseType: 'arraybuffer', observe: 'response'});
  }

  deleteBook(id: number) {
    return this.httpClient.delete(this.url + '/books/delete/' + id);
  }

  getAllUsers() {
    return this.httpClient.get<UserDTO[]>(this.url + '/users/all');
  }

  createUser(createUserDTO: CreateUserDTO) {
    return this.httpClient.post(this.url + '/users/create', createUserDTO);
  }

  changeUser(changeUserDTO: ChangeUserDTO) {
    return this.httpClient.put(this.url + '/users/change', changeUserDTO);
  }

  updateUser(updateUserDTO: UpdateUserDTO) {
    return this.httpClient.put(this.url + '/users/update-user', updateUserDTO);
  }

  updatePassword(updatePasswordDTO: UpdatePasswordDTO) {
    return this.httpClient.put(this.url + '/users/update-password', updatePasswordDTO);
  }

  deleteUser(id: number) {
    return this.httpClient.delete(this.url + '/users/delete/' + id);
  }

  getAllCategories() {
    return this.httpClient.get<CategoryDTO[]>(this.url + '/categories/all');
  }

  createCategory(createCategoryDTO: CreateCategoryDTO) {
    return this.httpClient.post(this.url + '/categories/create', createCategoryDTO);
  }

  updateCategory(updateCategoryDTO: UpdateCategoryDTO) {
    return this.httpClient.put(this.url + '/categories/update', updateCategoryDTO);
  }

  deleteCategory(id: number) {
    return this.httpClient.delete(this.url + '/categories/delete/' + id);
  }

}
