import {Injectable} from '@angular/core';
import {User} from './model/user.model';

@Injectable()
export class DataService {

  public user: User;
  public loggedIn: boolean;

  constructor() {
  }

}
