export class UploadBookDTO {
  private _id: number;

  private _title: string;

  private _author: string;

  private _keywords: string;

  private _publicationYear: number;

  private _mime: string;

  private _languageId: number;

  private _categoryId: number;

  constructor(id: number, title: string, author: string,
              keywords: string, publicationYear: number,
              MIME: string, languageId: number, categoryId: number) {
    this._id = id;
    this._title = title;
    this._author = author;
    this._keywords = keywords;
    this._publicationYear = publicationYear;
    this._mime = MIME;
    this._languageId = languageId;
    this._categoryId = categoryId;
  }

  get id(): number {
    return this._id;
  }

  set id(value: number) {
    this._id = value;
  }

  get title(): string {
    return this._title;
  }

  set title(value: string) {
    this._title = value;
  }

  get author(): string {
    return this._author;
  }

  set author(value: string) {
    this._author = value;
  }

  get keywords(): string {
    return this._keywords;
  }

  set keywords(value: string) {
    this._keywords = value;
  }

  get publicationYear(): number {
    return this._publicationYear;
  }

  set publicationYear(value: number) {
    this._publicationYear = value;
  }

  get mime(): string {
    return this._mime;
  }

  set mime(value: string) {
    this._mime = value;
  }

  get languageId(): number {
    return this._languageId;
  }

  set languageId(value: number) {
    this._languageId = value;
  }

  get categoryId(): number {
    return this._categoryId;
  }

  set categoryId(value: number) {
    this._categoryId = value;
  }
}
