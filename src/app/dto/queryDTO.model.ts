export class QueryDTO {
  public title: string;

  public author: string;

  public keywords: string;

  public publicationYear: number;

  public categoryId: number;

  public languageId: number;

  public content: string;

  constructor(title?: string, author?: string, keywords?: string, publicationYear?: number,
              categoryId?: number, languageId?: number, content?: string) {
    this.title = title;
    this.author = author;
    this.keywords = keywords;
    this.publicationYear = publicationYear;
    this.categoryId = categoryId;
    this.languageId = languageId;
    this.content = content;
  }
}
