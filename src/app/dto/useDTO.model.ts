import {CategoryDTO} from './categoryDTO.model';

export class UserDTO {
  private _id: number;

  private _firstName: string;

  private _lastName: string;

  private _username: string;

  private _type: string;

  private _category: CategoryDTO;

  constructor(id: number, firstName: string, lastName: string, username: string, type: string, category: CategoryDTO) {
    this._id = id;
    this._firstName = firstName;
    this._lastName = lastName;
    this._username = username;
    this._type = type;
    this._category = category;
  }


  get id(): number {
    return this._id;
  }

  get firstName(): string {
    return this._firstName;
  }

  get lastName(): string {
    return this._lastName;
  }

  get username(): string {
    return this._username;
  }

  get type(): string {
    return this._type;
  }

  get category(): CategoryDTO {
    return this._category;
  }


  set id(value: number) {
    this._id = value;
  }

  set firstName(value: string) {
    this._firstName = value;
  }

  set lastName(value: string) {
    this._lastName = value;
  }

  set username(value: string) {
    this._username = value;
  }

  set type(value: string) {
    this._type = value;
  }

  set category(value: CategoryDTO) {
    this._category = value;
  }
}
