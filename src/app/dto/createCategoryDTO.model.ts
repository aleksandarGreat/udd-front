export class CreateCategoryDTO {
  private name: string;

  constructor(name: string) {
    this.name = name;
  }
}
