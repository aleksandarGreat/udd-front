import {CategoryDTO} from './categoryDTO.model';
import {LanguageDTO} from './languageDTO.model';

export class BookDTO {
  private _id: number;

  private _title: string;

  private _author: string;

  private _keywords: string;

  private _publicationYear: number;

  private _mime: string;

  private _category: CategoryDTO;

  private _language: LanguageDTO;

  private _content: string;

  constructor(id: number, title: string, author: string, keywords: string, publicationYear: number,
              MIME: string, category: CategoryDTO, language: LanguageDTO, content: string) {
    this._id = id;
    this._title = title;
    this._author = author;
    this._keywords = keywords;
    this._publicationYear = publicationYear;
    this._mime = MIME;
    this._category = category;
    this._language = language;
    this._content = content;
  }

  get id(): number {
    return this._id;
  }

  set id(value: number) {
    this._id = value;
  }

  get title(): string {
    return this._title;
  }

  set title(value: string) {
    this._title = value;
  }

  get author(): string {
    return this._author;
  }

  set author(value: string) {
    this._author = value;
  }

  get keywords(): string {
    return this._keywords;
  }

  set keywords(value: string) {
    this._keywords = value;
  }

  get publicationYear(): number {
    return this._publicationYear;
  }

  set publicationYear(value: number) {
    this._publicationYear = value;
  }

  get mime(): string {
    return this._mime;
  }

  set mime(value: string) {
    this._mime = value;
  }

  get category(): CategoryDTO {
    return this._category;
  }

  set category(value: CategoryDTO) {
    this._category = value;
  }

  get language(): LanguageDTO {
    return this._language;
  }

  set language(value: LanguageDTO) {
    this._language = value;
  }

  get content(): string {
    return this._content;
  }

  set content(value: string) {
    this._content = value;
  }
}
