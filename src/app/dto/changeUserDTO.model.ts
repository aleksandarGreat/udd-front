export class ChangeUserDTO {
  private id: number;

  private username: string;

  private password: string;

  private firstName: string;

  private lastName: string;

  private type: number;

  private categoryId: number;

  constructor(id: number, username: string, password: string, firstName: string, lastName: string, type: number, categoryId: number) {
    this.id = id;
    this.username = username;
    this.password = password;
    this.firstName = firstName;
    this.lastName = lastName;
    this.type = type;
    this.categoryId = categoryId;
  }
}
