export class CreateUserDTO {
  private username: string;

  private password: string;

  private firstName: string;

  private lastName: string;

  private type: number;

  private categoryId: number;

  constructor(username: string, password: string, firstName: string, lastName: string, type: number, categoryId: number) {
    this.username = username;
    this.password = password;
    this.firstName = firstName;
    this.lastName = lastName;
    this.type = type;
    this.categoryId = categoryId;
  }
}
