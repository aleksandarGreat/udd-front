export class UpdatePasswordDTO {
  private id: number;

  private oldPassword: string;

  private newPassword: string;

  private retypedPassword: string;

  constructor(id: number, oldPassword: string, newPassword: string, retypedPassword: string) {
    this.id = id;
    this.oldPassword = oldPassword;
    this.newPassword = newPassword;
    this.retypedPassword = retypedPassword;
  }
}
