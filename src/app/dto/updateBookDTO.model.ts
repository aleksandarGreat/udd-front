export class UpdateBookDTO {
  public id: number;

  public title: string;

  public author: string;

  public keywords: string;

  public publicationYear: number;

  public mime: string;

  public categoryId: number;

  public languageId: number;

  constructor(id?: number, title?: string, author?: string,
              keywords?: string, publicationYear?: number,
              mime?: string, categoryId?: number, languageId?: number) {
    this.id = id;
    this.title = title;
    this.author = author;
    this.keywords = keywords;
    this.publicationYear = publicationYear;
    this.mime = mime;
    this.categoryId = categoryId;
    this.languageId = languageId;
  }
}
