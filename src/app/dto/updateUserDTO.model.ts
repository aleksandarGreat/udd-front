export class UpdateUserDTO {
  private id: number;

  private newUsername: string;

  private firstName: string;

  private lastName: string;

  constructor(id: number, newUsername: string, firstName: string, lastName: string) {
    this.id = id;
    this.newUsername = newUsername;
    this.firstName = firstName;
    this.lastName = lastName;
  }
}
