import {Component, OnInit} from '@angular/core';
import {RestService} from '../rest.service';
import {UserDTO} from '../dto/useDTO.model';
import {DataService} from '../data.service';
import {CategoryDTO} from '../dto/categoryDTO.model';
import {CreateUserDTO} from '../dto/createUserDTO.model';
import {type} from 'os';
import {ChangeUserDTO} from '../dto/changeUserDTO.model';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

  users: Array<UserDTO> = [];
  categories: Array<CategoryDTO> = [];

  addUser = false;
  userChanging = false;

  userId: number;
  username: string;
  password: string;
  firstName: string;
  lastName: string;
  categoryId: number;
  type: number;

  constructor(private restService: RestService, private dataService: DataService) {
  }

  ngOnInit() {
    this.getUsers();

    this.restService.getAllCategories().subscribe(
      res => {
        this.categories = res;
      },
      err => {
        console.log(err);
      }
    );
  }

  private getUsers() {
    this.restService.getAllUsers().subscribe(
      res => {
        console.log(res);
        this.users = res;
        this.users.forEach( (item, index) => {
          if (item.id === this.dataService.user.id) {
            this.users.splice(index, 1);
          }
        });
      },
      err => {
        console.log(err);
      }
    );
  }

  cancel() {
    this.addUser = false;
    this.userChanging = false;
  }

  add() {
    this.addUser = true;
  }

  createUser() {
    const createUserDTO = new CreateUserDTO(this.username, this.password, this.firstName, this.lastName, this.type, this.categoryId);
    console.log(createUserDTO);

    this.restService.createUser(createUserDTO).subscribe(
      res => {
        this.addUser = false;
        console.log(res);
        this.getUsers();
      },
      err => {
        console.log(err);
      }
    );
  }

  change(user: UserDTO) {
    this.userChanging = true;
    this.userId = user.id;
    this.username = user.username;
    this.firstName = user.firstName;
    this.lastName = user.lastName;
    if (user.category != null) {
      this.categoryId = user.category.id;
    }

    if (user.type === 'ADMINISTRATOR') {
      this.type = 0;
    } else {
      this.type = 1;
    }
  }

  changeUser() {
    const changeUserDTO = new ChangeUserDTO(this.userId, this.username, this.password,
                                            this.firstName, this.lastName, this.type, this.categoryId);
    console.log(changeUserDTO);

    this.restService.changeUser(changeUserDTO).subscribe(
      res => {
        this.userChanging = false;
        console.log(res);
        this.getUsers();
      },
      err => {
        console.log(err);
      }
    );
  }

  delete(id: number) {
    this.restService.deleteUser(id).subscribe(
      res => {
        console.log(res);
        this.users.forEach( (item, index) => {
          if (item.id === id) {
            this.users.splice(index, 1);
          }
        });
      },
      err => {
        console.log(err);
      }
    );
  }
}
