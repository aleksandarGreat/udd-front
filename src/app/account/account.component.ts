import {Component, OnInit} from '@angular/core';
import {DataService} from '../data.service';
import {User} from '../model/user.model';
import {RestService} from '../rest.service';
import {UpdatePasswordDTO} from '../dto/updatePasswordDTO.model';
import {UpdateUserDTO} from '../dto/updateUserDTO.model';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss']
})
export class AccountComponent implements OnInit {

  user: User;
  isUserUpdated = false;
  isPasswordUpdated = false;
  passwordsNotMatching = false;

  username: string;
  firstName: string;
  lastName: string;

  oldPassword: string;
  newPassword: string;
  retypedPassword: string;

  changePasswordError = 'Passwords did not match!';

  constructor(public dataService: DataService, private restService: RestService) {
  }

  ngOnInit() {
    this.user = this.dataService.user;
    this.username = this.dataService.user.username;
    this.firstName = this.dataService.user.firstName;
    this.lastName = this.dataService.user.lastName;
  }

  cancel() {
    this.isUserUpdated = false;
    this.isPasswordUpdated = false;
  }

  updateUser() {
    this.isUserUpdated = true;
    this.isPasswordUpdated = false;
  }

  changeUser() {
    if (this.dataService.user.username === this.username &&
        this.dataService.user.firstName === this.firstName &&
        this.dataService.user.lastName === this.lastName) {
      this.isUserUpdated = false;
    } else {
      const updateUserDTO = new UpdateUserDTO(this.dataService.user.id, this.username, this.firstName, this.lastName);
      console.log(updateUserDTO);

      this.restService.updateUser(updateUserDTO).subscribe(
        res => {
          console.log(res);
          this.dataService.user.username = this.username;
          this.dataService.user.firstName = this.firstName;
          this.dataService.user.lastName = this.lastName;
          this.isUserUpdated = false;
        },
        err => {
          console.log(err);
          this.passwordsNotMatching = true;
          this.changePasswordError = err.error;
        }
      );
    }
  }

  updatePassword() {
    this.isPasswordUpdated = true;
    this.isUserUpdated = false;
  }

  changePassword() {
    this.passwordsNotMatching = this.newPassword !== this.retypedPassword;
    this.changePasswordError = 'Passwords did not match!';

    if (!this.passwordsNotMatching) {
      const updatePasswordDTO = new UpdatePasswordDTO(this.dataService.user.id, this.oldPassword, this.newPassword, this.retypedPassword);
      console.log(updatePasswordDTO);

      this.restService.updatePassword(updatePasswordDTO).subscribe(
        res => {
          console.log(res);
          this.isPasswordUpdated = false;
        },
        err => {
          this.passwordsNotMatching = true;
          this.changePasswordError = err.error;
        }
      );
    }
  }

}
