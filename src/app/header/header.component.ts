import { Component, OnInit } from '@angular/core';
import {DataService} from '../data.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(public dataService: DataService, private router: Router) { }

  ngOnInit() {
  }

  logout() {
    this.dataService.loggedIn = false;
    this.dataService.user = null;
    if (this.router.navigate([''])) {
      console.log('Logged out successfully');
    }
  }

}
